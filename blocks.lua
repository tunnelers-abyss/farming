local S = farming.intllib

minetest.register_node("farming:sugar_block", {
		description = S("Sugar Block"),
		tiles = {"farming_sugar_block.png"},
		groups = {snappy = 3},
		sounds = default.node_sound_sand_defaults(),
})

minetest.register_craft({
	output = "farming:sugar_block",
	recipe = {
		{'farming:sugar', 'farming:sugar', 'farming:sugar'},
		{'farming:sugar', 'farming:sugar', 'farming:sugar'},
		{'farming:sugar', 'farming:sugar', 'farming:sugar'},
	}
})

minetest.register_craft({
	output = "farming:sugar 9",
	recipe = {
		{"farming:sugar_block"},
	}
})

minetest.register_node("farming:chocolate_block", {
		description = S("Chocolate Block"),
		tiles = {"farming_chocolate_block.png"},
		groups = {snappy = 3},
		sounds = default.node_sound_dirt_defaults(),
})

minetest.register_craft({
	output = "farming:chocolate_block",
	recipe = {
		{'farming:chocolate_dark', 'farming:chocolate_dark', 'farming:chocolate_dark'},
		{'farming:chocolate_dark', 'farming:chocolate_dark', 'farming:chocolate_dark'},
		{'farming:chocolate_dark', 'farming:chocolate_dark', 'farming:chocolate_dark'},
	}
})

minetest.register_craft({
	output = "farming:chocolate_dark 9",
	recipe = {
		{"farming:chocolate_block"},
	}
})

