--[[
	Farming Redo Mod
	by TenPlus1
	NEW growing routine by prestidigitator
	auto-refill by crabman77
]]

farming = {
	mod = "redo",
	version = "20231206",
	path = minetest.get_modpath("farming"),
	select = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, -5/16, 0.5}
	},
	select_final = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, -2.5/16, 0.5}
	},
	registered_plants = {},
	min_light = 12,
	max_light = 15,
	mapgen = minetest.get_mapgen_setting("mg_name"),
	use_utensils = minetest.settings:get_bool("farming_use_utensils") ~= false,
	mtg = minetest.get_modpath("default"),
	mcl = minetest.get_modpath("mcl_core"),
	sounds = {}
}

-- default sound functions just incase
function farming.sounds.node_sound_defaults() end
function farming.sounds.node_sound_glass_defaults() end
function farming.sounds.node_sound_gravel_defaults() end
function farming.sounds.node_sound_leaves_defaults() end
function farming.sounds.node_sound_stone_defaults() end
function farming.sounds.node_sound_wood_defaults() end

-- sounds check
if farming.mtg then farming.sounds = default end
if farming.mcl then farming.sounds = mcl_sounds end

-- check for creative mode or priv
local creative_mode_cache = minetest.settings:get_bool("creative_mode")

function farming.is_creative(name)
	return creative_mode_cache or minetest.check_player_privs(name, {creative = true})
end


local statistics = dofile(farming.path .. "/statistics.lua")

-- Translation support
local S = minetest.get_translator("farming")

farming.translate = S

-- localise
local random = math.random
local floor = math.floor

-- Utility Function
local time_speed = tonumber(minetest.settings:get("time_speed")) or 72
local SECS_PER_CYCLE = (time_speed > 0 and (24 * 60 * 60) / time_speed) or 0
local function clamp(x, min, max)
	return (x < min and min) or (x > max and max) or x
end


-- return amount of day or night that has elapsed
-- dt is time elapsed, count_day if true counts day, otherwise night
local function day_or_night_time(dt, count_day)

	local t_day = minetest.get_timeofday()
	local t1_day = t_day - dt / SECS_PER_CYCLE
	local t1_c, t2_c  -- t1_c < t2_c and t2_c always in [0, 1)

	if count_day then

		if t_day < 0.25 then
			t1_c = t1_day + 0.75  -- Relative to sunup, yesterday
			t2_c = t_day  + 0.75
		else
			t1_c = t1_day - 0.25  -- Relative to sunup, today
			t2_c = t_day  - 0.25
		end
	else
		if t_day < 0.75 then
			t1_c = t1_day + 0.25  -- Relative to sundown, yesterday
			t2_c = t_day  + 0.25
		else
			t1_c = t1_day - 0.75  -- Relative to sundown, today
			t2_c = t_day  - 0.75
		end
	end

	local dt_c = clamp(t2_c, 0, 0.5) - clamp(t1_c, 0, 0.5)  -- this cycle

	if t1_c < -0.5 then
		local nc = floor(-t1_c)
		t1_c = t1_c + nc
		dt_c = dt_c + 0.5 * nc + clamp(-t1_c - 0.5, 0, 0.5)
	end

	return dt_c * SECS_PER_CYCLE
end


--- Useful constants
-- Average time per growth stage.
local STAGE_LENGTH_AVG = tonumber(
		minetest.settings:get("farming_stage_length")) or 500

-- Standard deviation used in randomization of growth stage time.
local STAGE_LENGTH_DEV = STAGE_LENGTH_AVG / 6

-- Metadata key of stored game time when growth should trigger.
local META_TIME_KEY = "farming:growth_time"

-- Generates a stage length using the normal distribution and its global
-- parameters.
local function gen_stage_length()
	local stage_length = statistics.normal(STAGE_LENGTH_AVG, STAGE_LENGTH_DEV)

	stage_length = clamp(stage_length, 0.5 * STAGE_LENGTH_AVG, 3.0 * STAGE_LENGTH_AVG)

	return stage_length
end

-- Support for dynamic speed changes is not implemented.
local time_speed = tonumber(minetest.settings:get("time_speed"))
assert(time_speed)

-- Restarts timer at given position using given duration or randomized stage
-- length and manages node metadata accordingly.
--
-- Parameters:
-- pos: Vector;
--   Position of the node.
-- dur: nil or number;
--   If a number, use it as timer duration.
--   If nil, a growth stage duration is generated.
-- use_meta_time: nil or bool;
--   If truthy, uses stored growth time as base, otherwise uses current game
--   time. If stored growth time does not exist or is 0, the current time will
--   be used instead.
--
-- If there is no stored growth time, it is assumed to be 0.
--
-- A minimum duration of one second from now is enforced by clamping the value.
local function restart_growth_timer(pos, dur, use_meta_time)
	local timer = minetest.get_node_timer(pos)

	if not dur then
		dur = gen_stage_length()
	end

	local meta = minetest.get_meta(pos)

	local now = minetest.get_gametime()

	local timer_dur
	local base_time

	if not use_meta_time then
		base_time = now
		timer_dur = dur
	else
		base_time = meta:get_int(META_TIME_KEY)
		if base_time == 0 then
			base_time = now
		end
	end

	local new_growth_time = base_time + math.floor(dur)

	if use_meta_time then
		-- This can be negative.
		timer_dur = new_growth_time - now
	end

	if not (timer_dur >= 1.0) then
		-- Enforce minimum duration.
		timer_dur = 1.0
	end

	timer:start(timer_dur)
	meta:set_int(META_TIME_KEY, new_growth_time)

	-- Not super secret, but needless bloat for clients.
	meta:mark_as_private(META_TIME_KEY)
end

-- quick start seed
farming.start_seed_timer = function(pos)
	restart_growth_timer(pos, nil, false)
end

-- Timers must be stopped explicitly on node destruct.
local function stop_timer_on_destruct(pos)
	minetest.get_node_timer(pos):stop()
end

-- Registers a stage of planth growth.
--
-- To be called for each stage that grows to another node, in other words, teh
-- final stage needs to not be registered.
--
-- This functions asserts that the node is defined and has the appropriate group
-- assigned.
--
-- Parameters:
-- node_name: string;
--   Name of the node to be registered.
-- grows_into: string;
--   Name of the node the registered node turns into.
function farming.register_growth_stage(node_name, grows_into)
	local node_def = minetest.registered_nodes[node_name]

	assert(node_def, "Node " .. node_name .. " is not defined!")

	if not (minetest.get_item_group(node_name, "growing") > 0) then
		assert(false, "Node " .. node_name ..  " is not in `growing` group!")
	end

	if node_def.on_timer then
		minetest.log("warning", "[farming] Overriding `on_timer` in node " .. node_name)
	end

	local tab = {
		next_plant = grows_into,

		on_timer = farming.plant_growth_timer,
	}

	local old_constr = node_def.on_construct
	local old_destr  = node_def.on_destruct

	if type(old_constr) == "function" then
		tab.on_construct = function(pos)
			old_constr(pos)
			farming.handle_growth(pos)
		end
	else
		tab.on_construct = farming.handle_growth
	end

	if type(old_destr) == "function" then
		tab.on_destruct = function(pos)
			minetest.get_node_timer(pos):stop()
			old_destr(pos)
		end
	else
		tab.on_destruct = stop_timer_on_destruct
	end

	minetest.override_item(node_name, tab)
end

-- Helper for registering a number of consecutive stages.
--
-- Parameters:
-- base_name: string;
--   Common prefix of the nodes, excluding the underscore
-- before to be appended number.
-- max_stage: number;
--   The highest stage that will turn into into another node. This is usually
--   one less than the final stage!
function farming.register_growth_stages(base_name, max_stage)
	for stage = 1, max_stage do
		local node_name = base_name .. "_" .. stage
		local grows_into = base_name .. "_" .. (stage + 1)

		farming.register_growth_stage(node_name, grows_into)
	end
end

-- Detects a crop at given position, starting or stopping growth timer when needed
--
-- Parameters:
-- pos: vector;
--   Position of the node.
-- node: nil or string or table;
--   Optional name or node table of the node. A node table is only required to
--   have a `name` property with type string.
--
-- `node` is automatically determined if not provided.
function farming.handle_growth(pos, node)
	if not pos then
		return
	end

	local node_name
	if type(node) == "string" then
		node_name = node
	elseif type(node) == "table" then
		node_name = node.name
	else
		node_name = minetest.get_node(pos).name
	end

	ndef = minetest.registered_nodes[node_name]

	if not ndef then
		-- Unknown node
		return
	end

	local next_node = ndef.next_plant
	local timer = minetest.get_node_timer(pos)

	if not next_node then
		timer:stop()
		return
	end

	if not timer:is_started() then
		restart_growth_timer(pos, nil, true)
	end
end

-- Just in case a growing type or added node is missed (also catches existing
-- nodes added to map before timers were incorporated).
minetest.register_abm({
	label = "Start crop timer",
	nodenames = {"group:growing"},
	interval = 300,
	chance = 1,
	catch_up = false,
	action = farming.handle_growth
})

-- Plant timer function that grows plants under the right conditions.
--
-- Parameters:
-- pos: vector;
--   Position of the node.
-- _elapsed;
--   Ignored. Exists for explicit compatibility to be provided as `on_timer`
--   callback.
function farming.plant_growth_timer(pos, _elapsed)
	local node = minetest.get_node(pos)
	local ndef = minetest.registered_nodes[node.name]

	if not ndef then
		return
	end

	local next_node = ndef.next_plant

	if not next_node then
		return
	end

	local meta = minetest.get_meta(pos)
	-- Game time when growth should trigger.
	local growth_time = meta:get_int("farming:growth_time")
	local current_time = minetest.get_gametime()

	-- Due to precision issues it is common that the timer triggers just before
	-- the time according to our own logic is reached. One is added to give it a
	-- little bit of slack.
	if growth_time > current_time + 1 then
		-- This was not supposd to run yet, try again when stored time is reached.
		restart_growth_timer(pos, 0.0, true)
		return
	end 

	-- custom growth check
	local chk = ndef.growth_check

	if chk then
		if not chk(pos, node_name) then
			restart_growth_timer(pos, nil, false)
			return
		end
	else
		-- otherwise check for wet soil beneath crop
		local under = minetest.get_node({x = pos.x, y = pos.y - 1, z = pos.z})

		if minetest.get_item_group(under.name, "soil") < 3 then
			if under.name == "ignore" then
				-- The node is probably simply not loaded yet. Retry in a short while.
				restart_growth_timer(pos, 5.0, true)
			else
				restart_growth_timer(pos, nil, false)
			end

			return
		end
	end

	local MIN_LIGHT = ndef.minlight or farming.min_light
	local MAX_LIGHT = ndef.maxlight or farming.max_light

	local growth_daytime = nil
	if (current_time - growth_time) > 60 and time_speed > 0 then
		-- Only consider historic daylight when obviously catching up.

		local day_length = 86400 / time_speed
		local day_time = minetest.get_timeofday()
		-- Guaranteed to be greater than `growth_time`
		local next_day_start = current_time - day_length * day_time + day_length
		local time_diff = next_day_start - growth_time
		local growth_day_elapsed = day_length - time_diff % day_length
		growth_daytime = growth_day_elapsed / day_length
	end
	
	local light = minetest.get_node_light(pos, growth_daytime) or 0

	if light < MIN_LIGHT or light > MAX_LIGHT then
		restart_growth_timer(pos, nil, true)
		return
	end

	local next_stage_ndef = minetest.registered_nodes[next_node]
	if next_stage_ndef then
		local p2 = next_stage_ndef.place_param2 or 1

		minetest.swap_node(pos, {name = next_stage_ndef.name, param2 = p2})

		if not next_stage_ndef.next_plant then
			-- Plant has reached the final stage. Remove useless metadata.
			meta:set_string(META_TIME_KEY, "")
		else
			restart_growth_timer(pos, nil, true)
		end
	else
		minetest.log("error", "[farming] " .. (stages.next_stage.name) .. " expected to be registered, but is not")
		return
	end
end


-- refill placed plant by crabman (26/08/2015) updated by TenPlus1
function farming.refill_plant(player, plantname, index)

	if not player then return end

	local inv = player:get_inventory()

	if not inv then return end

	local old_stack = inv:get_stack("main", index)

	if old_stack:get_name() ~= "" then
		return
	end

	for i, stack in ipairs(inv:get_list("main")) do

		if stack:get_name() == plantname and i ~= index then

			inv:set_stack("main", index, stack)
			stack:clear()
			inv:set_stack("main", i, stack)

			return
		end
	end
end


-- Place Seeds on Soil
function farming.place_seed(itemstack, placer, pointed_thing, plantname)

	local pt = pointed_thing

	-- check if pointing at a node
	if not pt or pt.type ~= "node" then
		return
	end

	local under = minetest.get_node(pt.under)

	-- am I right-clicking on something that has a custom on_place set?
	-- thanks to Krock for helping with this issue :)
	local def = minetest.registered_nodes[under.name]
	if placer and itemstack and def and def.on_rightclick then
		return def.on_rightclick(pt.under, under, placer, itemstack, pt)
	end

	local above = minetest.get_node(pt.above)

	-- check if pointing at the top of the node
	if pt.above.y ~= pt.under.y + 1 then
		return
	end

	-- return if any of the nodes is not registered
	if not minetest.registered_nodes[under.name]
	or not minetest.registered_nodes[above.name] then
		return
	end

	-- can I replace above node, and am I pointing at soil
	if not minetest.registered_nodes[above.name].buildable_to
	or minetest.get_item_group(under.name, "soil") < 2
	-- avoid multiple seed placement bug
	or minetest.get_item_group(above.name, "plant") ~= 0 then
		return
	end

	-- is player planting seed?
	local name = placer and placer:get_player_name() or ""

	-- if not protected then add node and remove 1 item from the itemstack
	if not minetest.is_protected(pt.above, name) then

		local p2 = minetest.registered_nodes[plantname].place_param2 or 1

		minetest.set_node(pt.above, {name = plantname, param2 = p2})

		minetest.sound_play("default_place_node", {pos = pt.above, gain = 1.0})

		if placer and itemstack
		and not farming.is_creative(placer:get_player_name()) then

			local name = itemstack:get_name()

			itemstack:take_item()

			-- check for refill
			if itemstack:get_count() == 0 then

				minetest.after(0.2,
					farming.refill_plant,
					placer,
					name,
					placer:get_wield_index()
				)
			end
		end

		return itemstack
	end
end


-- Function to register plants (default farming compatibility)
farming.register_plant = function(name, def)

	if not def.steps then
		return nil
	end

	local mname = name:split(":")[1]
	local pname = name:split(":")[2]

	-- Check def
	def.description = def.description or S("Seed")
	def.inventory_image = def.inventory_image or "unknown_item.png"
	def.minlight = def.minlight or 12
	def.maxlight = def.maxlight or 15

	local base_name = mname .. ":" .. pname
	local seed_name = mname .. ":seed_" .. pname

	-- Register seed
	minetest.register_node(":" .. seed_name, {

		description = def.description,
		tiles = {def.inventory_image},
		inventory_image = def.inventory_image,
		wield_image = def.inventory_image,
		drawtype = "signlike",
		groups = {
			seed = 1, snappy = 3, attached_node = 1, flammable = 2, growing = 1,
			compostability = 65, handy = 1
		},
		paramtype = "light",
		paramtype2 = "wallmounted",
		walkable = false,
		sunlight_propagates = true,
		selection_box = farming.select,
		place_param2 = 1, -- place seed flat
		next_plant = base_name .. "_1",

		on_place = function(itemstack, placer, pointed_thing)
			return farming.place_seed(itemstack, placer, pointed_thing,
					seed_name)
		end
	})

	-- Register harvest
	minetest.register_craftitem(":" .. base_name, {
		description = pname:gsub("^%l", string.upper),
		inventory_image = mname .. "_" .. pname .. ".png",
		groups = def.groups or {flammable = 2},
	})

	-- Register growing steps
	for i = 1, def.steps do

		local base_rarity = 1

		if def.steps ~= 1 then
			base_rarity =  8 - (i - 1) * 7 / (def.steps - 1)
		end

		local drop = {
			items = {
				{items = {mname .. ":" .. pname}, rarity = base_rarity},
				{items = {mname .. ":" .. pname}, rarity = base_rarity * 2},
				{items = {mname .. ":seed_" .. pname}, rarity = base_rarity},
				{items = {mname .. ":seed_" .. pname}, rarity = base_rarity * 2},
			}
		}

		local sel = farming.select
		local g = {
			handy = 1, snappy = 3, flammable = 2, plant = 1, growing = 1,
			attached_node = 1, not_in_creative_inventory = 1,
		}

		-- Last step doesn't need growing=1 so Abm never has to check these
		-- also increase selection box for visual indication plant has matured
		if i == def.steps then
			sel = farming.select_final
			g.growing = 0
		end

		local node_name = base_name .. "_" .. i

		local next_plant = nil

		if i < def.steps then
			next_plant = base_name .. "_" .. (i + 1)
		end

		minetest.register_node(node_name, {
			drawtype = "plantlike",
			waving = 1,
			tiles = {mname .. "_" .. pname .. "_" .. i .. ".png"},
			paramtype = "light",
			paramtype2 = def.paramtype2,
			place_param2 = def.place_param2,
			walkable = false,
			buildable_to = true,
			sunlight_propagates = true,
			drop = drop,
			selection_box = sel,
			groups = g,
			sounds = farming.sounds.node_sound_leaves_defaults(),
			minlight = def.minlight,
			maxlight = def.maxlight,
			next_plant = next_plant
		})

		if next_plant then
			farming.register_growth_stage(node_name, next_plant)
		end
	end

	-- Must be done after the loop as the nodes need to be registered first.
	farming.register_growth_stage(seed_name, base_name .. "_1")

	-- add to farming.registered_plants
	farming.registered_plants[base_name] = {
		crop = base_name,
		seed = seed_name,
		steps = def.steps,
		minlight = def.minlight,
		maxlight = def.maxlight
	}
--print(dump(farming.registered_plants[mname .. ":" .. pname]))
	-- Return info
	return {seed = seed_name, harvest = base_name}
end


-- default settings
farming.asparagus = 0.002
farming.eggplant = 0.002
farming.spinach = 0.002
farming.carrot = 0.001
farming.potato = 0.001
farming.tomato = 0.001
farming.cucumber = 0.001
farming.corn = 0.001
farming.coffee = 0.001
farming.melon = 0.001
farming.pumpkin = 0.001
farming.cocoa = true
farming.raspberry = 0.001
farming.blueberry = 0.001
farming.rhubarb = 0.001
farming.beans = 0.001
farming.grapes = 0.001
farming.barley = true
farming.chili = 0.003
farming.hemp = 0.003
farming.garlic = 0.001
farming.onion = 0.001
farming.pepper = 0.002
farming.pineapple = 0.001
farming.peas = 0.001
farming.beetroot = 0.001
farming.vanilla = 0.002
farming.mint = 0.005
farming.cabbage = 0.001
farming.blackberry = 0.002
farming.soy = 0.001
farming.vanilla = 0.001
farming.lettuce = 0.001
farming.artichoke = 0.001
farming.parsley = 0.002
farming.sunflower = 0.001
farming.ginger = 0.002
farming.strawberry = not minetest.get_modpath("ethereal") and 0.002
farming.grains = true
farming.rice = true


-- Load new global settings if found inside mod folder
local input = io.open(farming.path .. "/farming.conf", "r")
if input then
	dofile(farming.path .. "/farming.conf")
	input:close()
end

-- load new world-specific settings if found inside world folder
local worldpath = minetest.get_worldpath()
input = io.open(worldpath .. "/farming.conf", "r")
if input then
	dofile(worldpath .. "/farming.conf")
	input:close()
end

-- recipe items
dofile(farming.path .. "/items.lua")

-- important items
if minetest.get_modpath("default") then
	dofile(farming.path .. "/soil.lua")
	dofile(farming.path .. "/hoes.lua")
end

dofile(farming.path.."/grass.lua")
dofile(farming.path.."/utensils.lua")

-- default crops
if not farming.mcl then
	dofile(farming.path.."/crops/wheat.lua")
end

dofile(farming.path.."/crops/cotton.lua")

-- disable crops Mineclone already has
if farming.mcl then
	farming.carrot = nil
	farming.potato = nil
	farming.melon = nil
	farming.cocoa = nil
	farming.beetroot = nil
	farming.sunflower = nil
	farming.pumpkin = nil
end

--Custom overrides for TA
farming.spinach = nil
farming.parsley = nil
farming.sunflower = nil
farming.cucina_vegana = nil
farming.ginger = nil
farming.eggplant = nil
farming.soy = nil
farming.asparagus = nil
farming.mint = nil

-- helper function
local function ddoo(file, check)

	if check then
		dofile(farming.path .. "/crops/" .. file)
	end
end

-- add additional crops and food (if enabled)
ddoo("carrot.lua", farming.carrot)
ddoo("potato.lua", farming.potato)
ddoo("tomato.lua", farming.tomato)
ddoo("cucumber.lua", farming.cucumber)
ddoo("corn.lua", farming.corn)
ddoo("coffee.lua", farming.coffee)
ddoo("melon.lua", farming.melon)
ddoo("pumpkin.lua", farming.pumpkin)
ddoo("cocoa.lua", farming.cocoa)
ddoo("raspberry.lua", farming.raspberry)
ddoo("blueberry.lua", farming.blueberry)
ddoo("rhubarb.lua", farming.rhubarb)
ddoo("beans.lua", farming.beans)
ddoo("grapes.lua", farming.grapes)
ddoo("barley.lua", farming.barley)
ddoo("hemp.lua", farming.hemp)
ddoo("garlic.lua", farming.garlic)
ddoo("onion.lua", farming.onion)
ddoo("pepper.lua", farming.pepper)
ddoo("pineapple.lua", farming.pineapple)
ddoo("peas.lua", farming.peas)
ddoo("beetroot.lua", farming.beetroot)
ddoo("chili.lua", farming.chili)
ddoo("ryeoatrice.lua", farming.grains)
ddoo("rice.lua", farming.rice)
ddoo("mint.lua", farming.mint)
ddoo("cabbage.lua", farming.cabbage)
ddoo("blackberry.lua", farming.blackberry)
ddoo("soy.lua", farming.soy)
ddoo("vanilla.lua", farming.vanilla)
ddoo("lettuce.lua", farming.lettuce)
ddoo("artichoke.lua", farming.artichoke)
ddoo("parsley.lua", farming.parsley)
ddoo("sunflower.lua", farming.sunflower)
ddoo("strawberry.lua", farming.strawberry)
ddoo("asparagus.lua", farming.asparagus)
ddoo("eggplant.lua", farming.eggplant)
ddoo("spinach.lua", farming.spinach)
ddoo("ginger.lua", farming.ginger)

dofile(farming.path .. "/food.lua")

if not farming.mcl then
	dofile(farming.path .. "/compatibility.lua") -- Farming Plus compatibility
end

if minetest.get_modpath("lucky_block") then
	dofile(farming.path .. "/lucky_block.lua")
end

print("[MOD] Farming Redo loaded")
