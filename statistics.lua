local statistics = {}
local ROOT_2 = math.sqrt(2.0)

local A = 8 * (math.pi - 3.0) / (3.0 * math.pi * (4.0 - math.pi))
local B = 4.0 / math.pi
local C = 2.0 / (math.pi * A)
local D = 1.0 / A

local function erf_inv(x)

	if x == 0 then return 0; end

	if x <= -1 or x >= 1 then return nil; end

	local y = math.log(1 - x * x)
	local u = C + 0.5 * y
	local v = math.sqrt(math.sqrt(u * u - D * y) - u)

	return (x > 0 and v) or -v
end


local function std_normal(u)
	return ROOT_2 * erf_inv(2.0 * u - 1.0)
end

-- Inverse error function.
statistics.erf_inv = erf_inv

--- Standard normal distribution function (mean 0, standard deviation 1).
 --
 -- @return
 --    Any real number (actually between -3.0 and 3.0).

statistics.std_normal = function()

	local u = math.random()

	if u < 0.001 then
		return -3.0
	elseif u > 0.999 then
		return 3.0
	end

	return std_normal(u)
end


--- Standard normal distribution function (mean 0, standard deviation 1).
 --
 -- @param mu
 --    The distribution mean.
 -- @param sigma
 --    The distribution standard deviation.
 -- @return
 --    Any real number (actually between -3*sigma and 3*sigma).

statistics.normal = function(mu, sigma)

	local u = math.random()

	if u < 0.001 then
		return mu - 3.0 * sigma
	elseif u > 0.999 then
		return mu + 3.0 * sigma
	end

	return mu + sigma * std_normal(u)
end

return statistics
